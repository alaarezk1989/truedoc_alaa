<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html data-editor-version="2" class="sg-campaigns" xmlns="http://www.w3.org/1999/xhtml">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1"/>
    <!--[if !mso]><!-->
    <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
    <!--<![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
    <![endif]-->
    <!--[if (gte mso 9)|(IE)]>
    <style type="text/css">
        body {
            width: 600px;
            margin: 0 auto;
        }

        table {
            border-collapse: collapse;
        }

        table, td {
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
        }

        img {
            -ms-interpolation-mode: bicubic;
        }
    </style>
    <![endif]-->

    <style type="text/css">
        body,
        p,
        div {
            font-family: helvetica, arial, sans-serif;
            font-size: 16px;
        }

        body {
            color: #8d9db9;
        }

        body a {
            color: #fe5d61;
            text-decoration: none;
        }

        p {
            margin: 0;
            padding: 0;
        }

        table.wrapper {
            width: 100% !important;
            table-layout: fixed;
            -webkit-font-smoothing: antialiased;
            -webkit-text-size-adjust: 100%;
            -moz-text-size-adjust: 100%;
            -ms-text-size-adjust: 100%;
        }

        img.max-width {
            max-width: 100% !important;
        }

        .column.of-2 {
            width: 50%;
        }

        .column.of-3 {
            width: 33.333%;
        }

        .column.of-4 {
            width: 25%;
        }

        @media screen and (max-width: 480px) {

            .preheader .rightColumnContent,
            .footer .rightColumnContent {
                text-align: left !important;
            }

            .preheader .rightColumnContent div,
            .preheader .rightColumnContent span,
            .footer .rightColumnContent div,
            .footer .rightColumnContent span {
                text-align: left !important;
            }

            .preheader .rightColumnContent,
            .preheader .leftColumnContent {
                font-size: 80% !important;
                padding: 5px 0;
            }

            table.wrapper-mobile {
                width: 100% !important;
                table-layout: fixed;
            }

            img.max-width {
                height: auto !important;
                max-width: 480px !important;
            }

            a.bulletproof-button {
                display: block !important;
                width: auto !important;
                font-size: 80%;
                padding-left: 0 !important;
                padding-right: 0 !important;
            }

            .columns {
                width: 100% !important;
            }

            .column {
                display: block !important;
                width: 100% !important;
                padding-left: 0 !important;
                padding-right: 0 !important;
                margin-left: 0 !important;
                margin-right: 0 !important;
            }
        }
    </style>
</head>

<body>

<div class="webkit">
    <table cellpadding="0" cellspacing="0" border="0" width="100%" class="wrapper" bgcolor="#f2f4fb">
        <tr>
            <td valign="top" bgcolor="#f2f4fb" width="100%">
                <table width="100%" role="content-container" class="outer" align="center" cellpadding="0"
                       cellspacing="0" border="0">
                    <tr>
                        <td width="100%">
                            <table width="100%" cellpadding="0" cellspacing="0" border="0">
                                <tr>
                                    <td>
                                        <center>
                                            <table>
                                                <tr>
                                                    <td width="600">
                                                        <table width="100%" cellpadding="0" cellspacing="0" border="0"
                                                               style="width: 100%; max-width:600px;" align="center">
                                                            <tr>
                                                                <td role="modules-container"
                                                                    style="padding: 0px 0px 0px 0px; color: #8d9db9; text-align:left;"
                                                                    bgcolor="#f2f4fb" width="100%" align="left">
                                                                    <table class="wrapper" role="module"
                                                                           data-type="image" border="0" cellpadding="0"
                                                                           cellspacing="0" width="100%"
                                                                           style="table-layout: fixed;">

                                                                    </table>
                                                                    <table class="wrapper" role="module"
                                                                           data-type="image" border="0" cellpadding="0"
                                                                           cellspacing="0" width="100%"
                                                                           style="table-layout: fixed;">
                                                                        <tr>
                                                                            <td style="font-size:6px;line-height:10px;padding:0px
                                                        0px 0px 0px;" valign="top" align="center"></td>
                                                                        </tr>
                                                                        select_file                                         </table>

                                                                    <table class="module" role="module" data-type="text"
                                                                           border="0" cellpadding="0" cellspacing="0"
                                                                           width="100%" style="table-layout: fixed;">

                                                                    </table>
                                                                    <table class="module" role="module" data-type="code"
                                                                           border="0" cellpadding="0" cellspacing="0"
                                                                           width="100%" style="table-layout: fixed;">
                                                                        <tr>
                                                                            <td height="100%" valign="top">
                                                                                <div class="col-lg-12"
                                                                                     style="background-color:#F8F8FB;padding:20px">
                                                                                    <div style="text-align: right;">
                                                                                        <span
                                                                                            style="font-family: 'Sarabun',sans-serif; line-height:20px;"><span
                                                                                                style="color:#29294D;"> Accepted Recorders: {{ $data['acceptedRows'] }}</span></span>
                                                                                    </div>
                                                                                    <div style="text-align: right;">
                                                                                        <span
                                                                                            style="font-family: 'Sarabun',sans-serif; line-height:20px;"><span
                                                                                                style="color:#29294D;"> Rejected Recorders: {{ $data['rejectedRows'] }}</span></span>
                                                                                    </div>
                                                                                    <div style="text-align: right;">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                    <div style="text-align: right;">
                                                                                        &nbsp;
                                                                                    </div>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                    <table class="module" role="module" data-type="code"
                                                                           border="0" cellpadding="0" cellspacing="0"
                                                                           width="100%" style="table-layout: fixed;">
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </center>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>

</body>
</html>
