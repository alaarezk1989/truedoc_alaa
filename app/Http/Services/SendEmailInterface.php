<?php

namespace App\Http\Services;

interface SendEmailInterface
{
    public function sendEmail($email, $data);
}
