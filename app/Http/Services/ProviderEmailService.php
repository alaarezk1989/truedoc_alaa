<?php

namespace App\Http\Services;

use App\Mail\ImportUsersMail;
use Illuminate\Support\Facades\Mail;

class ProviderEmailService implements SendEmailInterface
{
    public function sendEmail($email, $data)
    {
        Mail::to('alaarezk1989@gmail.com')->send(new ImportUsersMail($data));

//        Mail::to($email)->send(new ImportUsersMail($data));
    }
}
