<?php

namespace App\Http\Services;

use Maatwebsite\Excel\Concerns\ToModel;
use Illuminate\Contracts\Queue\ShouldQueue;
Use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithBatchInserts;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;


class ImportService implements ToModel, WithStartRow, WithBatchInserts, WithChunkReading, ShouldQueue, WithEvents
{
    public $model;
    public $columns;
    public $acceptedRows = 0;
    public $rejectedRows = 0;
    public $sendEmailService;

    public function __construct($model, $columns, SendEmailInterface $sendEmailService)
    {
        $this->model = $model;
        $this->columns = $columns;
        $this->sendEmailService = $sendEmailService;
    }

    public function model(array $row)
    {
        $data = [];
        $i = 0;
        foreach ($this->columns as $key => $value) {
            $data[$value] = $row[$i];
            $i++;
        }

        if ($data['first_name'] == "" || $data['second_name'] == "" || $data['family_name'] == "" || $data['uid'] == "") {
            ++$this->rejectedRows;
            $data = [];
        }

        if (!empty($data)) {
            ++$this->acceptedRows;
            return new $this->model($data);
        }
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    public function batchSize(): int
    {
        return 1000;
    }

    public function chunkSize(): int
    {
        return 1000;
    }

    public function registerEvents(): array
    {

        return [
            AfterSheet::class => function (AfterSheet $event) {
                $data = [
                    "acceptedRows" => $this->acceptedRows,
                    "rejectedRows" => $this->rejectedRows,
                ];

                $this->sendEmailService->sendEmail("alaarezk1989@gmail.com", $data);
            },
        ];
    }
}
