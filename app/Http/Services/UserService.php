<?php

namespace App\Http\Services;

use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class UserService
{
    protected $uploaderService;
    protected $sendEmailService;

    public function __construct(UploaderService $uploaderService, SendEmailInterface $sendEmailService)
    {
        $this->uploaderService = $uploaderService;
        $this->sendEmailService = $sendEmailService;
    }

    public function uploadAndImport(Request $request)
    {
        $model = 'App\Models\User';
        $columns = [
            'first_name',
            'second_name',
            'family_name',
            'uid',
        ];

        Excel::queueImport(new ImportService($model, $columns, $this->sendEmailService),
            request()->file('select_file'));

    }
}
