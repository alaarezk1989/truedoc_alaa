<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ImportRequest extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'select_file' => 'required|mimes:xls,xlsx',
        ] ;
    }

    public function messages()
    {
        return [
            'select_file.required' => "Please select an excel file",
        ] ;
    }
}
