<?php

namespace App\Http\Controllers;


use App\Mail\ImportUsersMail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {

        return view('welcome');
    }

}
