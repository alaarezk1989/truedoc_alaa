<?php

namespace App\Http\Controllers;

use App\Http\Services\UserService;
use Illuminate\Http\Request;
use App\Mail\ImportUsersMail;
use Illuminate\Support\Facades\Mail;
use App\Http\Controllers\Controller;

use View;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function import()
    {
//        $data = [
//            'acceptedRows' => 'import',
//            'rejectedRows' => 'vvvv',
//        ];
//        Mail::to('alaarezk1989@gmail.com')->send(new ImportUsersMail($data));

        return View::make('users.import');
    }

    public function storeImport(Request $request)
    {
        $this->userService->uploadAndImport($request);
        return back()->with('success', "Users data imported successfully");

    }

    public function apiStoreImport(Request $request)
    {
//        return $request->all();
        $this->userService->uploadAndImport($request);
        return response()->json('sucess', 200);
    }

}
