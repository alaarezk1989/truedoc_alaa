<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImportUserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testImportExcel()
    {

//            $filePath = env('APP_URL').'/tests/files/PHPTask.xlsx';
        $filePath = base_path('tests/files/PHPTask.xlsx');

        $response = $this->json('POST', '/api/users/import',
            ['select_file' => $filePath]);

        $response->dumpHeaders();

        $response->dump();

    }
}
